package ru.tsc.kitaev.tm.command.task;

import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.util.TerminalUtil;

import java.util.List;

public class TaskShowAllFromProjectByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "tasks-show-by-project-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show tasks by project id...";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getTaskService().findById(projectId) == null) throw new ProjectNotFoundException();
        final List<Task> taskUpdated = serviceLocator.getProjectTaskService().findTaskByProjectId(projectId);
        if (taskUpdated == null) throw new TaskNotFoundException();
        System.out.println(taskUpdated);
    }

}
