package ru.tsc.kitaev.tm.command.task;

import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by name...";
    }

    @Override
    public void execute() {
        System.out.println("Enter Name");
        final String name = TerminalUtil.nextLine();
        System.out.println("[REMOVE PROJECT BY NAME]");
        serviceLocator.getTaskService().removeByName(name);
        System.out.println("[OK]");
    }

}
