package ru.tsc.kitaev.tm.command.system;

import ru.tsc.kitaev.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandsShowCommand extends AbstractCommand {

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String arg() {
        return "-cmd";
    }

    @Override
    public String description() {
        return "Display list commands...";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            String commandName = command.name();
            if (commandName != null || !commandName.isEmpty())
                System.out.println(commandName + ": " + command.description());
        }
    }

}
