package ru.tsc.kitaev.tm.exception.empty;

import ru.tsc.kitaev.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error. Role is empty.");
    }

    public EmptyRoleException(String value) {
        super("Error" + value + " Role is empty.");
    }

}
