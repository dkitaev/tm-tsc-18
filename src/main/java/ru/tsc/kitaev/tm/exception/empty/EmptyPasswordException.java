package ru.tsc.kitaev.tm.exception.empty;

import ru.tsc.kitaev.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error. Password is empty.");
    }

    public EmptyPasswordException(String value) {
        super("Error" + value + " Password is empty.");
    }

}
