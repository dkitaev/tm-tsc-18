package ru.tsc.kitaev.tm.exception.entity;

import ru.tsc.kitaev.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error. Project not found.");
    }

}
