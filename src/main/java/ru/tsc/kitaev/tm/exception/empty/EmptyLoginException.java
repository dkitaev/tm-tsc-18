package ru.tsc.kitaev.tm.exception.empty;

import ru.tsc.kitaev.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error. Login is empty.");
    }

    public EmptyLoginException(String value) {
        super("Error" + value + " Login is empty.");
    }

}
